from flask import Flask, jsonify , render_template, request
import psycopg2
app = Flask(__name__)

students=[]
con = psycopg2.connect("host=localhost dbname=task user=postgres")
cur = con.cursor()


@app.route('/')
def index():
    cur.execute("select * from students")
    students1 = cur.fetchall()
    for row in students1:

        student = {
            'id': row[0],
            'name': row[1],
            'email': row[2]
        }
        students.append(student)
    return "hello student"


@app.route("/students",methods=['GET'])
def get():
   return jsonify({'Students':students})

@app.route('/students/<int:id>',methods=['GET'])
def get_Student(id):
    return  jsonify({'student':students[id]})

@app.route('/add')
def add_Student():
    return render_template('studentform.html')
@app.route('/', methods=['POST'])

def getValue():
   name=request.form['name']
   email=request.form['email']
   contact_number=request.form['contact_number']
   address=request.form['address']
   student={
           'id':len(students)+1,
          'name':name,
           'email':email
   }
   sql="INSERT INTO students(name,email) VALUES(%s,%s)"
   cur.execute(sql,(name,email))
   con.commit()
   students.append(student)
   return "add Successfully"


if __name__ == '__main__':
   app.run(debug = True)
