from flask import Flask, jsonify , render_template, request
app = Flask(__name__)

students=[{'id':"1",
           'name':"Shivani",
           'email':"shivani@gmail.com",
           },
          {'id': "2",
           'name': "Sakshi",
           'email': "sakshi@gmail.com",

           }
          ]

@app.route('/')
def primary():
    return "Hii.. there!!"


@app.route("/students",methods=['GET'])
def get():
   return jsonify({'Students':students})

@app.route('/students/<int:id>',methods=['GET'])
def get_Student(id):
    return  jsonify({'student':students[id]})

@app.route('/add')
def add_Student():
    return render_template('studentform.html')
@app.route('/', methods=['POST'])

def getValue():
   name=request.form['name']
   email=request.form['email']

   student={
           'id':len(students)+1,
          'name':name,
           'email':email,

   }
   students.append(student)
   return "add Successfully"


if __name__ == '__main__':
   app.run(debug = True)
